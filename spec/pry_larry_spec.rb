require_relative 'setup'
RSpec.describe Pry::Larry do
  let(:tester) do
    pry_tester(binding)
  end

  let(:hash_key) do
    tester.pry.hash
  end

  before do
    Pry::Larry.start(tester.pry)
  end

  it "prevents growth of Array(s) referenced by Pry::Larry::MEMORY" do
    5.times {|n| tester.eval(n.to_s) }
    expect(Pry::Larry::MEMORY[hash_key]).to be_empty
  end

  it "tells the user the duration of eval" do
    tester.pry.color = false
    tester.eval("sleep 0.1")
    expect(tester.out.string).to match(/Benchmark: 0\.1[0-9]s/)
  end

  describe "start subcommand" do
    it "tells the user that Larry has been enabled" do
      Pry::Larry.stop(tester.pry)
      tester.eval "larry start"
      expect(tester.out.string).to include("Alright, I have started benchmarking your code")
    end
  end

  describe "stop subcommand" do
    it "tells the user that Larry has been disabled" do
      tester.eval "larry stop"
      expect(tester.out.string).to include("Alright, I have stopped benchmarking your code")
    end
  end

  describe "larry-start alias" do
    specify "it works" do
      tester.eval "larry-start"
      expect(tester.out.string).to include("Alright, I have started benchmarking your code")
    end
  end

  describe "larry-stop alias" do
    specify "it works" do
      tester.eval "larry-stop"
      expect(tester.out.string).to include("Alright, I have stopped benchmarking your code")
    end
  end
end
