require 'bundler/setup'
require 'pry/testable'
Bundler.require :default, :test

RSpec.configure do |config|
  config.include Pry::Testable::Evalable
end
