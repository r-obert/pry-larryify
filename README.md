[![Build Status](https://travis-ci.org/r-obert/pry-larryify.rb.svg?branch=master)](https://travis-ci.org/r-obert/pry-larryify.rb)

Mirrors:

* [Github](https://github.com/r-obert/pry-larryify)
* [Gitlab](https://gitlab.com/r-obert/pry-larryify)

__pry-larryify__

Larry is a plugin for the [Pry](https://github.com/pry/pry) repl that
measures the duration of code (and optionally, Pry commands!) you enter
during a Pry session. No *manual* interaction required. The measurement of time
is wall clock time. Almost everything about Larry is configurable.

__Configuration__

Configuration isn't required unless you want to change the default behaviour.
For a permanent change, add a `.pryrc` file in your $HOME directory or in the
current working directory of your project:

```ruby
Pry.configure do |config|
  config.larry.auto_start = false             # Benchmark by default? Default is true
  config.larry.benchmark_commands = true      # Benchmark Pry commands? Default is false
  config.larry.speak_if = ->(pry, walltime) { # Print benchmark dependent on condition.
                                              # Default condition is print if walltime > 0.0
    walltime > 0.5
  }
  config.larry.speaker = ->(pry, walltime) {  # Present the benchmark results.
                                              # Default is: "Benchmark: %.2fs".
    pry.pager.page sprintf("Elapsed time: %.2fs", walltime)
  }
end
```

For temporary changes, while in the repl modify `_pry_.config.larry`.
That's all there is to it.

__Examples__

__1.__

    [1] pry(main)> sleep 2
    Benchmark: 2.00s
    => 2
    [2] pry(main)> larry stop
    Alright, I have stopped benchmarking your code.
    [3] pry(main)> sleep 2
    => 2

__2.__

    [1] pry(main)> def hard_work
    [1] pry(main)*   sleep rand(10)
    [1] pry(main)* end
    => :hard_work
    [2] pry(main)> hard_work
    Benchmark: 5.01s
    => 5

__Install__

Rubygems:

    $ gem install pry-larryify

Bundler:

```ruby
gem "pry-larryify"
```

__License__

[MIT](./LICENSE.txt).
