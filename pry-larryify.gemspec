require './lib/pry-larryify/version'
Gem::Specification.new do |spec|
  spec.name = "pry-larryify"
  spec.version = Pry::Larry::VERSION
  spec.authors = ["1xAB Software"]
  spec.email = "1xAB@protonmail.com"
  spec.summary = "Automated in-repl benchmarking tool as a Pry plugin"
  spec.description = spec.summary
  spec.homepage = "https://github.com/r-obert/pry-larryify"
  spec.licenses = ["MIT"]
  spec.require_paths = ["lib"]
  spec.files = Dir["*.{md,txt,gemspec}", "lib/**/*.rb"]
  spec.add_runtime_dependency "pry", "~> 0.11"
end
