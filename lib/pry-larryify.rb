require 'pry' if not defined?(Pry::ClassCommand)
module Pry::Larry
  MEMORY = Hash.new{|h,k| h[k] = [] }
  BEFORE_EVAL = ->(_, pry) do
    MEMORY[pry.hash].push Time.now.to_f
  end
  AFTER_EVAL = ->(input, pry) do
    walltime = (sprintf "%.2f", Time.now.to_f - MEMORY[pry.hash][-1]).to_f
    if input.nil? # Pry command
      pry.config.larry.benchmark_commands and
      pry.config.larry.speak_if.call(pry, walltime) and
      pry.config.larry.speaker.call(pry, walltime)
    else
      pry.config.larry.speak_if.call(pry, walltime) and
      pry.config.larry.speaker.call(pry, walltime)
    end
    MEMORY[pry.hash].clear
  end
  BEFORE_SESSION = ->(_,_, pry) do
    Pry::Larry.start(pry) if pry.config.larry.auto_start
  end
  AFTER_SESSION = ->(_, _, pry) do
    MEMORY.delete(pry.hash)
  end

  def self.start(pry)
    if not pry.config.hooks.hook_exists? :before_eval, BEFORE_EVAL.hash
      pry.config.hooks.add_hook :before_eval, BEFORE_EVAL.hash, BEFORE_EVAL
      pry.config.hooks.add_hook :after_eval,  AFTER_EVAL.hash , AFTER_EVAL
      pry.config.hooks.add_hook :after_session, AFTER_SESSION.hash , AFTER_SESSION
    end
  end

  def self.stop(pry)
    pry.config.hooks.delete_hook :before_eval, BEFORE_EVAL.hash
    pry.config.hooks.delete_hook :after_eval , AFTER_EVAL.hash
    pry.config.hooks.delete_hook :after_session , AFTER_SESSION.hash
    MEMORY[pry.hash].clear
  end

  class << self
    alias_method :wakeup, :start
    alias_method :chill, :stop
  end

  class LarryCommand < Pry::ClassCommand
    match /larry (start|wakeup|stop|chill|-h|--help)/
    group 'pry-larry'
    description 'Ask larry to start or stop counting wall-clock time.'
    banner <<-CMDBANNER
    Usage: larry [start|stop]

    Ask Larry to start or stop benchmarking your code (or Pry commands).
    CMDBANNER

    START_COMMANDS = ["start", "wakeup"]

    def process(command)
      if START_COMMANDS.include?(command)
        Pry::Larry.start(_pry_)
        _pry_.pager.page("Alright, I have started benchmarking your code.")
      else
        Pry::Larry.stop(_pry_)
        _pry_.pager.page("Alright, I have stopped benchmarking your code.")
      end
    end
  end

  Pry::Commands.add_command(LarryCommand)
  Pry::Commands.alias_command "larry-stop", "larry stop"
  Pry::Commands.alias_command "larry-start", "larry start"
  Pry.config.hooks.add_hook :before_session, BEFORE_SESSION.hash, BEFORE_SESSION
  Pry.config.larry = Pry::Config.from_hash({
    auto_start: true,
    benchmark_commands: false,
    speak_if: ->(pry, walltime) { walltime > 0 },
    speaker: ->(pry, walltime) {
      pry.pager.page "%{LarrySays} %{WallTime}s" % {
        :LarrySays => pry.color ? Pry::Helpers::Text.green("Benchmark:") : "Benchmark:",
        :WallTime  => sprintf("%.2f", walltime)
      }
    }
  }, nil)
end
